from django.shortcuts import render
from .forms import RegistrationForm
from .models import User
from django.http import JsonResponse
import json

def index(req):
    form = RegistrationForm()

    users = list(User.objects.all())
    return render(req, 'lab_10/index.html', {'form': form, 'users' : users})

def regist(req):
    if req.method == 'POST' and req.is_ajax():
        req = json.loads(req.body)

        new_user = User()
        new_user.name = req['name']
        new_user.email = req['email']
        new_user.password = req['password']
        new_user.save()

        return JsonResponse({'form_is_posted' : True})

def validate(req):
    if req.method == 'POST' and req.is_ajax():
        req = json.loads(req.body)

        is_exist = False
        is_valid = False

        if User.objects.filter(email = req['email']).first():
            is_exist = True

        form = RegistrationForm({'name': req['name'], 'password': req['password'], 'email': req['email']})

        if form.is_valid():
            is_valid = True

        return JsonResponse({'email_is_exist' : is_exist, 'form_is_valid': is_valid})

def unsub(req):
    if req.method == 'POST' and req.is_ajax():
        req = json.loads(req.body)

        print(req)
        pass_match = False
        email_found = False

        user = User.objects.filter(email = req['email']).first()

        if user:
            email_found = True
            if user.password == req['password']:
                pass_match = True
                user.delete()

        return JsonResponse({'pass_match' : pass_match, 'email_found' : email_found})
        
            



        


        
