from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def index(req):
    
    if req.method == 'POST' and req.is_ajax():
        response = json.loads(req.body)
        state = response['state']
        count = int(response['count'])
        
        if state == 'True':
            count -= 1
            state = 'False'
        else:
            count += 1
            state = 'True'
        return JsonResponse({'count' : str(count), 'state' : state})
    else:
        response = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
        data = response.json()
        
        return render(req, 'lab_9/index.html', {'book_data' : data['items']})

def search(req):

    if req.method == 'POST' and req.is_ajax():
        response = json.loads(req.body)
        key = response['key']

        key_url = 'https://www.googleapis.com/books/v1/volumes?q=' + key

        response = requests.get(key_url)
        key_json = response.json()

        return JsonResponse({'data' : key_json['items']})