from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class NewVisitorStatusTest(unittest.TestCase):  
    def setUp(self):  
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):  
        self.browser.quit()

    def test_retrieve_landing(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6')
        self.assertIn('Hello!', self.browser.title)

        inputbox = self.browser.find_element_by_id('id_status')
        self.assertEqual(inputbox.get_attribute('placeholder'), 'New Status')

        triggerModal = self.browser.find_element_by_class_name('btn')
        triggerModal.click();
        time.sleep(3);

        inputbox.send_keys('Coba Coba')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)

        entries = self.browser.find_element_by_class_name('row').text
        self.assertIn('Coba Coba', entries)

    # Test header inside container
    def test_html_hierarchy_first(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6')
        self.assertTrue(self.browser.find_element_by_css_selector("div.container > header.text-center").is_displayed())

    # Test form inside modal
    def test_html_hierarchy_second(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6')

        triggerModal = self.browser.find_element_by_class_name('btn')
        triggerModal.click();
        time.sleep(3);

        self.assertTrue(self.browser.find_element_by_css_selector("div.modal-body > form").is_displayed())

    def test_css_landing_bg_color(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6')
        self.assertIn('rgba(153, 255, 0, 0.792)', self.browser.find_element_by_tag_name('body').value_of_css_property('background'))

    def test_css_landing_font_family(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6')
        self.assertIn('Montserrat', self.browser.find_element_by_tag_name('body').value_of_css_property('font-family'))

class NewVisitorProfilePage(unittest.TestCase):
    def setUp(self):  
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):  
        self.browser.quit()
    
    def test_retrieve_landing(self):
        self.browser.get('http://ppw-lab-tdd.herokuapp.com/lab_6/profile')
        self.assertIn('Hello!', self.browser.title)

        banner = self.browser.find_element_by_tag_name('h1').text
        self.assertEqual('Hello, Apa Kabar?', banner)

