from django.shortcuts import render, redirect
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
from datetime import datetime

def index(req):
    statuses = Status.objects.all()
    form = StatusForm()
    response = {'form' : form, 'statuses' : statuses}
    return render(req, 'lab_6/index.html', response)

def add_status(req):
    if req.method == 'POST':
        form = StatusForm(req.POST)
        if form.is_valid():
            statusObj = Status()
            statusObj.status = form.cleaned_data['status']
            statusObj.date = datetime.now()

            statusObj.save()
        return redirect('lab_6:index')

def profile(req):
    return render(req, 'lab_6/profile.html',)
