from django.db import models
from datetime import datetime

class Status(models.Model) :
    status = models.CharField(max_length = 300)
    date = models.DateTimeField(default=datetime.now())
