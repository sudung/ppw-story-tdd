from django.urls import path
from .views import *

app_name = 'lab_6'

urlpatterns = [
    path('', index, name='index'),
    path('add_status/', add_status, name='add_status'),
    path('profile/', profile, name='profile'),
]