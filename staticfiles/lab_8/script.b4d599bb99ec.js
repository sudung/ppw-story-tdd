$("body").LoadingOverlay("show", {
    background : "rgba(255, 255, 255, 1)",
    fade: [0, 200],
});

window.onload = function () {
    setTimeout(function(){
        $("body").LoadingOverlay("hide");
    }, 500);
}

var toggle = false
function changeTheme() {
    if (toggle) {
        document.getElementById('theme').setAttribute('href', "/static/lab_8/theme-1.css");
        document.getElementById('theme-changer').setAttribute('class', "btn btn-dark");
        toggle = false;
    } else {
        document.getElementById('theme').setAttribute('href', "/static/lab_8/theme-2.css");
        document.getElementById('theme-changer').setAttribute('class', "btn btn-light");
        toggle = true;
    }
}






