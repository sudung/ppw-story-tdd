function checkValid() {
    let email = $('#email-content').val();
    let name = $('#name-content').val();
    let password = $('#password-content').val();
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://localhost:8000/lab_10/validate/'),
        data : JSON.stringify({'email' : email, 'name' : name, 'password' : password,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            let emailExist = response.email_is_exist;
            let formValid = response.form_is_valid;

            if (emailExist || !formValid) {
                $('#submit-button').prop("disabled", true);
                $('#form-info').prop("hidden", false);

                if (!formValid) {
                    $('#form-info').text("Form not valid!");
                } else if (emailExist) {
                    $('#form-info').text("Email already exists!");
                }
            } else {
                $('#submit-button').prop("disabled", false);
                $('#form-info').prop("hidden", false);
                $('#form-info').text("Form checks out!");
            }
        },
    })
}

function postForm() {
    event.preventDefault(true);
    let email = $('#email-content').val();
    let name = $('#name-content').val();
    let password = $('#password-content').val();
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url:('http://localhost:8000/lab_10/regist/'),
        data : JSON.stringify({'email' : email, 'name' : name, 'password' : password,}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function(response) {
            let formIsPosted = response.form_is_posted;

            if (formIsPosted) {
                $('#submit-button').prop("disabled", true);
                $('#form-info').prop("hidden", false);
                $('#form-info').text("User subscribed!");
            }
        }
    })
}