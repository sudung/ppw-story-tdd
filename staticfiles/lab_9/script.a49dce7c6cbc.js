function favoriteAction(element) {
    var count = $("#counter").text();
    var state = $(element).attr('value');
    var token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    if (state == 'False') {
        $(element).attr('value', 'True');
        $(element).attr('class', 'btn btn-success btn-sm');
    } else {
        $(element).attr('value', 'False');
        $(element).attr('class', 'btn btn-light btn-sm');
    }

    $.ajax({
        method: 'POST',
        url:('http://localhost:8000/lab_9/'),
        data : {'count' : count, 'state' : state},
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success : function (response) {
            count = response.count
            console.log(response)
            $("#counter").text(count)
        },
    })
}